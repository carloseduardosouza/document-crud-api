# Document API

*API to CRUD brazilian documents CPF / CNPJ*

## Getting Started

Start this project using Docker:

```bash
$ docker-compose up
```

After running docker-compose up configure virtual host in local machine
`document-crud-api.com`, this host can be changed in docker-compose.yml file

## Unit Tests
Unit tests created for the Domain layer in tests/AppTest folder and coverage is created automatically per assemble file in tests/coverage
