<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;
use Zend\Expressive\MiddlewareFactory;

/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Handler\HomePageHandler::class, 'home');
 * $app->post('/album', App\Handler\AlbumCreateHandler::class, 'album.create');
 * $app->put('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.put');
 * $app->patch('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.patch');
 * $app->delete('/album/:id', App\Handler\AlbumDeleteHandler::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Handler\ContactHandler::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */
return function (Application $app, MiddlewareFactory $factory, ContainerInterface $container) : void {
    $app->post('/api/document', \App\Handler\CreateDocumentHandler::class, 'document.create');
    $app->put(
        '/api/document/{id}',
        [
            \Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware::class,
            \App\Handler\UpdateDocumentHandler::class],
        'document.update'
    );
    $app->delete(
        '/api/document/{id}',
        [
            \Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware::class,
            \App\Handler\DeleteDocumentHandler::class
        ],
        'document.delete'
    );
    $app->get('/api/document/list',\App\Handler\ListDocumentHandler::class,'document.list');
    $app->get('/api/document/{id}',\App\Handler\LoadDocumentHandler::class,'document.load');
    $app->get('/api/status',\App\Handler\ApplicationStatusHandler::class,'application.status');
};
