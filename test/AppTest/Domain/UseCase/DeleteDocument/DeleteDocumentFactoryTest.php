<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\DeleteDocument;

use App\Domain\Repository\DocumentRepository;
use App\Domain\UseCase\DeleteDocument\DeleteDocument;
use App\Domain\UseCase\DeleteDocument\DeleteDocumentFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class DeleteDocumentFactoryTest extends TestCase
{
    public function testSuccess()
    {
        $container = $this->prophesize(ContainerInterface::class);
        $repository = $this->prophesize(DocumentRepository::class);
        $container->get(DocumentRepository::class)->willReturn($repository->reveal());
        $factory = new DeleteDocumentFactory();
        $this->assertInstanceOf(DeleteDocument::class, $factory($container->reveal()));
    }
}
