<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\DeleteDocument;

use App\Domain\Model\Document;
use App\Domain\Repository\DocumentRepository;
use App\Domain\UseCase\DeleteDocument\DeleteDocument;
use App\Domain\UseCase\DeleteDocument\DeleteDocumentRequest;
use App\Domain\UseCase\DeleteDocument\DeleteDocumentResponse;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class DeleteDocumentTest extends TestCase
{
    public function testSuccess()
    {
        $document = $this->prophesize(Document::class);
        $repository = $this->prophesize(DocumentRepository::class);
        $repository->load('id')->willReturn($document->reveal());
        $repository->delete(Argument::type(Document::class));
        $request = $this->prophesize(DeleteDocumentRequest::class);
        $request->getId()->willReturn('id');
        $useCase = new DeleteDocument($repository->reveal());
        $response = $useCase->handle($request->reveal());
        $this->assertInstanceOf(DeleteDocumentResponse::class, $response);
    }

    /**
     * @expectedException \App\Domain\Exception\DomainViolationException
     * @expectedExceptionMessage document_not_found
     */
    public function testFailNotFound()
    {
        $repository = $this->prophesize(DocumentRepository::class);
        $repository->load('id')->willReturn(null);
        $request = $this->prophesize(DeleteDocumentRequest::class);
        $request->getId()->willReturn('id');
        $useCase = new DeleteDocument($repository->reveal());
        $useCase->handle($request->reveal());
    }
}
