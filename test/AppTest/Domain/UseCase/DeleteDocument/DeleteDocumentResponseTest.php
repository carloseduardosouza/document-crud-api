<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\DeleteDocument;

use App\Domain\UseCase\DeleteDocument\DeleteDocumentResponse;
use PHPUnit\Framework\TestCase;

class DeleteDocumentResponseTest extends TestCase
{
    public function testSuccess()
    {
        $response = new DeleteDocumentResponse('id');
        $this->assertEquals(['id' => 'id'], $response->getData());
        $this->assertEquals('document_deleted', $response->getMessage());
    }
}
