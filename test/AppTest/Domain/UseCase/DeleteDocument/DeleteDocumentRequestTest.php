<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\DeleteDocument;

use App\Domain\UseCase\DeleteDocument\DeleteDocumentRequest;
use PHPUnit\Framework\TestCase;

class DeleteDocumentRequestTest extends TestCase
{
    public function testSuccess()
    {
        $request = new DeleteDocumentRequest('id');
        $this->assertEquals('id', $request->getId());
        $this->assertEquals([], $request->toArray());
    }
}
