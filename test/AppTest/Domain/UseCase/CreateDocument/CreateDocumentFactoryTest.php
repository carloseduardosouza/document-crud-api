<?php

declare(strict_types=1);

namespace AppTest\Domain\UseCase\CreateDocument;

use App\Domain\Repository\DocumentRepository;
use App\Domain\UseCase\CreateDocument\CreateDocument;
use App\Domain\UseCase\CreateDocument\CreateDocumentFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class CreateDocumentFactoryTest extends TestCase
{
    public function testSuccess()
    {
        $container = $this->prophesize(ContainerInterface::class);
        $repository = $this->prophesize(DocumentRepository::class);
        $container->get(DocumentRepository::class)->willReturn($repository->reveal());
        $factory = new CreateDocumentFactory();
        $this->assertInstanceOf(CreateDocument::class, $factory($container->reveal()));
    }
}
