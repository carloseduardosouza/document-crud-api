<?php

declare(strict_types=1);

namespace AppTest\Domain\UseCase\CreateDocument;

use App\Domain\Model\Document;
use App\Domain\UseCase\CreateDocument\CreateDocumentResponse;
use PHPUnit\Framework\TestCase;

class CreateDocumentResponseTest extends TestCase
{
    public function testSuccess()
    {
        $data = ['id' => '1', 'type' => 'type', 'value' => 'value', 'blacklisted' => false];
        $document = $this->prophesize(Document::class);
        $document->toArray()
            ->willReturn($data);
        $response = new CreateDocumentResponse($document->reveal());
        $this->assertEquals($data, $response->getData());
        $this->assertEquals('document_created', $response->getMessage());
    }
}
