<?php

declare(strict_types=1);

namespace AppTest\Domain\UseCase\CreateDocument;

use App\Domain\UseCase\CreateDocument\CreateDocumentRequest;
use PHPUnit\Framework\TestCase;

class CreateDocumentRequestTest extends TestCase
{
    public function testSuccess()
    {
        $request = new CreateDocumentRequest('type', 'value', false);
        $this->assertEquals(['type' => 'type', 'value' => 'value', 'blacklisted' => false], $request->toArray());
    }
}
