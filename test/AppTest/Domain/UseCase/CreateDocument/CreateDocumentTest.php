<?php

declare(strict_types=1);

namespace AppTest\Domain\UseCase\CreateDocument;

use App\Domain\Enum\Type;
use App\Domain\Model\Document;
use App\Domain\Repository\DocumentRepository;
use App\Domain\UseCase\CreateDocument\CreateDocument;
use App\Domain\UseCase\CreateDocument\CreateDocumentRequest;
use App\Domain\UseCase\CreateDocument\CreateDocumentResponse;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class CreateDocumentTest extends TestCase
{
    public function testSuccess()
    {
        $repository = $this->prophesize(DocumentRepository::class);
        $repository->store(Argument::type(Document::class))->will(function ($args) {
            $document = $args[0];
            $document->setId('1');
            return $document;
        });
        $repository->exists(Argument::type(Document::class))
            ->willReturn(false);
        $data = ['type' => Type::CNPJ, 'value' => 'value', 'blacklisted' => false];
        $request = $this->prophesize(CreateDocumentRequest::class);
        $request->toArray()->willReturn($data);
        $useCase = new CreateDocument($repository->reveal());
        $response = $useCase->handle($request->reveal());
        $this->assertInstanceOf(CreateDocumentResponse::class, $response);
        $this->assertEquals(array_merge($data, ['id'=>'1']), $response->getData());
        $this->assertEquals('document_created', $response->getMessage());
    }

    /**
     * @expectedException \App\Domain\Exception\DomainViolationException
     * @expectedExceptionMessage document_already_exists
     */
    public function testFailAlreadyExists()
    {
        $repository = $this->prophesize(DocumentRepository::class);
        $repository->store(Argument::type(Document::class))->will(function ($args) {
            $document = $args[0];
            $document->setId('1');
            return $document;
        });
        $repository->exists(Argument::type(Document::class))
            ->willReturn(true);
        $data = ['type' => Type::CNPJ, 'value' => 'value', 'blacklisted' => false];
        $request = $this->prophesize(CreateDocumentRequest::class);
        $request->toArray()->willReturn($data);
        $useCase = new CreateDocument($repository->reveal());
        $useCase->handle($request->reveal());
    }
}
