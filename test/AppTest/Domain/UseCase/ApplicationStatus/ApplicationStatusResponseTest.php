<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\ApplicationStatus;

use App\Domain\UseCase\ApplicationStatus\ApplicationsStatusResponse;
use App\Domain\ValueObject\Uptime;
use PHPUnit\Framework\TestCase;

class ApplicationStatusResponseTest extends TestCase
{
    public function testSuccess()
    {
        $uptime = $this->prophesize(Uptime::class);
        $uptime->getTotalMinutes()->willReturn(1);
        $response = new ApplicationsStatusResponse($uptime->reveal(), 1, 1, 1, 1);
        $this->assertEquals(
            [
                'uptime' => 1,
                'insert' => 1,
                'update' => 1,
                'delete' => 1,
                'search' => 1
            ],
            $response->getData()
        );
        $this->assertEquals('success', $response->getMessage());
    }
}
