<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\ApplicationStatus;

use App\Domain\UseCase\ApplicationStatus\ApplicationStatusRequest;
use PHPUnit\Framework\TestCase;

class ApplicationStatusRequestTest extends TestCase
{
    public function testSuccess()
    {
        $request = new ApplicationStatusRequest();
        $this->assertEquals([], $request->toArray());
    }
}
