<?php

declare(strict_types=1);

namespace AppTest\Domain\UseCase\ApplicationStatus;

use App\Domain\Repository\AuditRepository;
use App\Domain\UseCase\ApplicationStatus\ApplicationsStatusResponse;
use App\Domain\UseCase\ApplicationStatus\ApplicationStatus;
use App\Domain\UseCase\ApplicationStatus\ApplicationStatusRequest;
use App\Domain\ValueObject\UptimeFactory;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class ApplicationStatusTest extends TestCase
{
    public function testSuccess()
    {
        $repository = $this->prophesize(AuditRepository::class);
        $repository->countInsert(Argument::type(\DateTime::class))->willReturn(1);
        $repository->countUpdate(Argument::type(\DateTime::class))->willReturn(1);
        $repository->countDelete(Argument::type(\DateTime::class))->willReturn(1);
        $repository->countSearch(Argument::type(\DateTime::class))->willReturn(1);
        $request = $this->prophesize(ApplicationStatusRequest::class);
        $useCase = new ApplicationStatus($repository->reveal(), UptimeFactory::factory());
        $response = $useCase->handle($request->reveal());
        $this->assertInstanceOf(ApplicationsStatusResponse::class, $response);
    }
}
