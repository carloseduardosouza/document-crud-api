<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\ApplicationStatus;

use App\Domain\Repository\AuditRepository;
use App\Domain\UseCase\ApplicationStatus\ApplicationStatus;
use App\Domain\UseCase\ApplicationStatus\ApplicationStatusFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class ApplicationStatusFactoryTest extends TestCase
{
    public function testSuccess()
    {
        $container = $this->prophesize(ContainerInterface::class);
        $repository = $this->prophesize(AuditRepository::class);
        $container->get(AuditRepository::class)->willReturn($repository->reveal());
        $factory = new ApplicationStatusFactory();
        $this->assertInstanceOf(ApplicationStatus::class, $factory($container->reveal()));
    }
}
