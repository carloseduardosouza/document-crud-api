<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\LoadDocument;

use App\Domain\Repository\DocumentRepository;
use App\Domain\UseCase\LoadDocument\LoadDocument;
use App\Domain\UseCase\LoadDocument\LoadDocumentFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class LoadDocumentFactoryTest extends TestCase
{
    public function testSuccess()
    {
        $container = $this->prophesize(ContainerInterface::class);
        $repository = $this->prophesize(DocumentRepository::class);
        $container->get(DocumentRepository::class)->willReturn($repository->reveal());
        $factory = new LoadDocumentFactory();
        $this->assertInstanceOf(LoadDocument::class, $factory($container->reveal()));
    }
}
