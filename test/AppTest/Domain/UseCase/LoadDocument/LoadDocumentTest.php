<?php

declare(strict_types=1);

namespace AppTest\Domain\UseCase\LoadDocument;

use App\Domain\Model\Document;
use App\Domain\Repository\DocumentRepository;
use App\Domain\UseCase\LoadDocument\LoadDocument;
use App\Domain\UseCase\LoadDocument\LoadDocumentRequest;
use App\Domain\UseCase\LoadDocument\LoadDocumentResponse;
use PHPUnit\Framework\TestCase;

class LoadDocumentTest extends TestCase
{
    public function testSuccess()
    {
        $document = $this->prophesize(Document::class);
        $repository = $this->prophesize(DocumentRepository::class);
        $repository->load('id')->willReturn($document->reveal());
        $request = $this->prophesize(LoadDocumentRequest::class);
        $request->getId()->willReturn('id');
        $useCase = new LoadDocument($repository->reveal());
        $response = $useCase->handle($request->reveal());
        $this->assertInstanceOf(LoadDocumentResponse::class, $response);
    }

    /**
     * @expectedException \App\Domain\Exception\DomainViolationException
     * @expectedExceptionMessage document_not_found
     */
    public function testFailNotFound()
    {
        $repository = $this->prophesize(DocumentRepository::class);
        $repository->load('id')->willReturn(null);
        $request = $this->prophesize(LoadDocumentRequest::class);
        $request->getId()->willReturn('id');
        $useCase = new LoadDocument($repository->reveal());
        $useCase->handle($request->reveal());
    }
}
