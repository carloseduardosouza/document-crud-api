<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\LoadDocument;

use App\Domain\UseCase\LoadDocument\LoadDocumentRequest;
use PHPUnit\Framework\TestCase;

class DeleteDocumentRequestTest extends TestCase
{
    public function testSuccess()
    {
        $request = new LoadDocumentRequest('id');
        $this->assertEquals('id', $request->getId());
        $this->assertEquals([], $request->toArray());
    }
}
