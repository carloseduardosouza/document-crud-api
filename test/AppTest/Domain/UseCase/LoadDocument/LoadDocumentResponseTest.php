<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\LoadDocument;

use App\Domain\Enum\Type;
use App\Domain\Model\Document;
use App\Domain\UseCase\LoadDocument\LoadDocumentResponse;
use PHPUnit\Framework\TestCase;

class LoadDocumentResponseTest extends TestCase
{
    public function testSuccess()
    {
        $data = ['id' => 'id', 'type' => Type::CNPJ, 'value' => '123456', 'blacklisted' => false];
        $document = $document = $this->prophesize(Document::class);
        $document->toArray()->willReturn($data);
        $response = new LoadDocumentResponse($document->reveal());
        $this->assertEquals($data, $response->getData());
        $this->assertEquals('document_loaded', $response->getMessage());
    }
}
