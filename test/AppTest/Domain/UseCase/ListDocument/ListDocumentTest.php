<?php

declare(strict_types=1);

namespace AppTest\Domain\UseCase\ListDocument;

use App\Domain\Enum\Type;
use App\Domain\Model\Document;
use App\Domain\Repository\DocumentRepository;
use App\Domain\UseCase\ListDocument\ListDocument;
use App\Domain\UseCase\ListDocument\ListDocumentRequest;
use App\Domain\UseCase\ListDocument\ListDocumentResponse;
use PHPUnit\Framework\TestCase;

class ListDocumentTest extends TestCase
{
    public function testSuccess()
    {
        $data = ['id' => 'id', 'type' => Type::CNPJ, 'value' => '123456', 'blacklisted' => false];
        $document = $this->prophesize(Document::class);
        $document->toArray()->willReturn($data);
        $repository = $this->prophesize(DocumentRepository::class);
        $repository->list([], [])->willReturn([$document->reveal()]);
        $request = $this->prophesize(ListDocumentRequest::class);
        $request->getFilter()->willReturn([]);
        $request->getOrder()->willReturn([]);
        $useCase = new ListDocument($repository->reveal());
        $response = $useCase->handle($request->reveal());
        $this->assertInstanceOf(ListDocumentResponse::class, $response);
        $this->assertEquals([$data], $response->getData());
    }
}
