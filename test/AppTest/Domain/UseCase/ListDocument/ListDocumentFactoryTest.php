<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\ListDocument;

use App\Domain\Repository\DocumentRepository;
use App\Domain\UseCase\ListDocument\ListDocument;
use App\Domain\UseCase\ListDocument\ListDocumentFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class ListDocumentFactoryTest extends TestCase
{
    public function testSuccess()
    {
        $container = $this->prophesize(ContainerInterface::class);
        $repository = $this->prophesize(DocumentRepository::class);
        $container->get(DocumentRepository::class)->willReturn($repository->reveal());
        $factory = new ListDocumentFactory();
        $this->assertInstanceOf(ListDocument::class, $factory($container->reveal()));
    }
}
