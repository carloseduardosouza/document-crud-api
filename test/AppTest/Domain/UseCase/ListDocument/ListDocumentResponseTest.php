<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\ListDocument;

use App\Domain\Enum\Type;
use App\Domain\Model\Document;
use App\Domain\UseCase\ListDocument\ListDocumentResponse;
use PHPUnit\Framework\TestCase;

class ListDocumentResponseTest extends TestCase
{
    public function testSuccess()
    {
        $data = ['id' => 'id', 'type' => Type::CNPJ, 'value' => '123456', 'blacklisted' => false];
        $document = $document = $this->prophesize(Document::class);
        $document->toArray()->willReturn($data);
        $response = new ListDocumentResponse([$document->reveal()]);
        $this->assertEquals([$data], $response->getData());
        $this->assertEquals('documents_listed', $response->getMessage());
    }
}
