<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\ListDocument;

use App\Domain\Enum\Type;
use App\Domain\UseCase\ListDocument\ListDocumentRequest;
use PHPUnit\Framework\TestCase;

class ListDocumentRequestTest extends TestCase
{
    public function testSuccess()
    {
        $request = new ListDocumentRequest(Type::CNPJ, '123456', false, 'value');
        $this->assertEquals(['type' => Type::CNPJ, 'value' => '123456', 'blacklisted' => false], $request->getFilter());
        $this->assertEquals(['value' => 1], $request->getOrder());
        $this->assertEquals([], $request->toArray());
    }
}
