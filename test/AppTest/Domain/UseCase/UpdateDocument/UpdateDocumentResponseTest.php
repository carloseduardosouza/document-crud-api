<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\UpdateDocument;

use App\Domain\Model\Document;
use App\Domain\UseCase\UpdateDocument\UpdateDocumentResponse;
use PHPUnit\Framework\TestCase;

class UpdateDocumentResponseTest extends TestCase
{
    public function testSuccess()
    {
        $data = ['id' => '1', 'type' => 'type', 'value' => 'value', 'blacklisted' => false];
        $document = $this->prophesize(Document::class);
        $document->toArray()
            ->willReturn($data);
        $response = new UpdateDocumentResponse($document->reveal());
        $this->assertEquals($data, $response->getData());
        $this->assertEquals('document_updated', $response->getMessage());
    }
}
