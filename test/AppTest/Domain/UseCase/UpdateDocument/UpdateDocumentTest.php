<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\UpdateDocument;

use App\Domain\Enum\Type;
use App\Domain\Model\Document;
use App\Domain\Repository\DocumentRepository;
use App\Domain\UseCase\UpdateDocument\UpdateDocument;
use App\Domain\UseCase\UpdateDocument\UpdateDocumentRequest;
use App\Domain\UseCase\UpdateDocument\UpdateDocumentResponse;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class UpdateDocumentTest extends TestCase
{
    public function testSuccess()
    {
        $document = $this->prophesize(Document::class);
        $document->setType(Type::CNPJ)->willReturn($document);
        $document->setValue('value')->willReturn($document);
        $document->setBlacklisted(false)->willReturn($document);
        $repository = $this->prophesize(DocumentRepository::class);
        $repository->load('id')->willReturn($document->reveal());
        $repository->isUniq(Argument::type(Document::class))
            ->willReturn(true);
        $repository->update(Argument::type(Document::class))->willReturn($document);
        $data = ['type' => Type::CNPJ, 'value' => 'value', 'blacklisted' => false];
        $request = $this->prophesize(UpdateDocumentRequest::class);
        $request->toArray()->willReturn($data);
        $request->getId()->willReturn('id');
        $useCase = new UpdateDocument($repository->reveal());
        $response = $useCase->handle($request->reveal());
        $this->assertInstanceOf(UpdateDocumentResponse::class, $response);
    }

    /**
     * @expectedException \App\Domain\Exception\DomainViolationException
     * @expectedExceptionMessage document_not_found
     */
    public function testFailNotFound()
    {
        $repository = $this->prophesize(DocumentRepository::class);
        $repository->load('id')->willReturn(null);
        $request = $this->prophesize(UpdateDocumentRequest::class);
        $request->getId()->willReturn('id');
        $useCase = new UpdateDocument($repository->reveal());
        $useCase->handle($request->reveal());
    }

    /**
     * @expectedException \App\Domain\Exception\DomainViolationException
     * @expectedExceptionMessage document_is_not_uniq
     */
    public function testFailIsNotUniq()
    {
        $document = $this->prophesize(Document::class);
        $document->setType(Type::CNPJ)->willReturn($document);
        $document->setValue('value')->willReturn($document);
        $document->setBlacklisted(false)->willReturn($document);
        $repository = $this->prophesize(DocumentRepository::class);
        $repository->load('id')->willReturn($document->reveal());
        $repository->isUniq(Argument::type(Document::class))
            ->willReturn(false);
        $data = ['type' => Type::CNPJ, 'value' => 'value', 'blacklisted' => false];
        $request = $this->prophesize(UpdateDocumentRequest::class);
        $request->toArray()->willReturn($data);
        $request->getId()->willReturn('id');
        $useCase = new UpdateDocument($repository->reveal());
        $useCase->handle($request->reveal());
    }
}
