<?php
declare(strict_types=1);

namespace AppTest\Domain\UseCase\UpdateDocument;

use App\Domain\UseCase\UpdateDocument\UpdateDocumentRequest;
use PHPUnit\Framework\TestCase;

class UpdateDocumentRequestTest extends TestCase
{
    public function testSuccess()
    {
        $request = new UpdateDocumentRequest('id', 'type', 'value', false);
        $this->assertEquals(['type' => 'type', 'value' => 'value', 'blacklisted' => false], $request->toArray());
        $this->assertEquals('id', $request->getId());
    }
}
