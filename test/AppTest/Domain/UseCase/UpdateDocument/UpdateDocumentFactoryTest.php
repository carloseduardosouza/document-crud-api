<?php

declare(strict_types=1);

namespace AppTest\Domain\UseCase\UpdateDocument;

use App\Domain\Repository\DocumentRepository;
use App\Domain\UseCase\UpdateDocument\UpdateDocument;
use App\Domain\UseCase\UpdateDocument\UpdateDocumentFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class UpdateDocumentFactoryTest extends TestCase
{
    public function testSuccess()
    {
        $container = $this->prophesize(ContainerInterface::class);
        $repository = $this->prophesize(DocumentRepository::class);
        $container->get(DocumentRepository::class)->willReturn($repository->reveal());
        $factory = new UpdateDocumentFactory();
        $this->assertInstanceOf(UpdateDocument::class, $factory($container->reveal()));
    }
}
