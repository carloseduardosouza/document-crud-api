<?php
declare(strict_types=1);

namespace AppTest\Domain\ValueObject;

use App\Domain\ValueObject\Uptime;
use PHPUnit\Framework\TestCase;

class UptimeTest extends TestCase
{
    public function testSuccess()
    {
        $entity = new Uptime(1950.00);
        $this->assertEquals(1950, $entity->__toString());
        $this->assertEquals(32.5, $entity->getTotalMinutes());
    }
}
