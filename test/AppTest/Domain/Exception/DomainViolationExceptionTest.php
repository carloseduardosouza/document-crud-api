<?php
declare(strict_types=1);

namespace AppTest\Domain\Exception;

use App\Domain\Exception\DomainViolationException;
use PHPUnit\Framework\TestCase;

class DomainViolationExceptionTest extends TestCase
{
    public function testSuccess()
    {
        $exception = new DomainViolationException();
        $this->assertEquals(['message' => 'domain_violation_error'], $exception->getData());
    }
}
