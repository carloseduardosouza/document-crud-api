<?php
declare(strict_types=1);

namespace AppTest\Domain\Exception;

use App\Domain\Exception\InvalidRequestDataException;
use PHPUnit\Framework\TestCase;

class InvalidRequestDataExceptionTest extends TestCase
{
    public function testSuccess()
    {
        $exception = new InvalidRequestDataException(['id' => 'required']);
        $this->assertEquals(['message' => 'validation_error', 'errors' => ['id' => 'required']], $exception->getData());
    }
}
