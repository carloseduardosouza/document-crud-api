<?php
declare(strict_types=1);

namespace AppTest\Domain\Model;

use App\Domain\Enum\Type;
use App\Domain\Model\Document;
use PHPUnit\Framework\TestCase;

class CreateDocumentTest extends TestCase
{
    public function testSuccess()
    {
        $data = ['id' => 'id', 'type' => Type::CNPJ, 'value' => 'value', 'blacklisted' => false];
        $entity = new Document($data);
        $this->assertEquals(Type::CNPJ, $entity->getType());
        $this->assertEquals('value', $entity->getValue());
        $this->assertEquals(false, $entity->isBlacklisted());
        $this->assertEquals('id', $entity->getId());
        $this->assertEquals($data, $entity->toArray());
    }

    /**
     * @expectedException \App\Domain\Exception\DomainViolationException
     * @expectedExceptionMessage invalid_document_type
     */
    public function testFailAlreadyExists()
    {
        new Document(['id' => 'id', 'type' => 'type', 'value' => 'value', 'blacklisted' => false]);
    }
}
