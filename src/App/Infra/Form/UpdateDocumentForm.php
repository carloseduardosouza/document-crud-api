<?php
declare(strict_types=1);

namespace App\Infra\Form;

use App\Domain\Enum\Type;
use App\Infra\Validator\Cnpj;
use App\Infra\Validator\Cpf;
use Zend\Form\Form;

class UpdateDocumentForm extends Form
{
    /**
     * UpdateDocumentForm constructor.
     */
    public function __construct()
    {
        parent::__construct('update-document');
        $this->setInputFilter(new UpdateDocumentFilter());
        $this->add(['name' => 'id']);
        $this->add(['name' => 'type']);
        $this->add(['name' => 'value']);
        $this->add(['name' => 'blacklisted']);
    }

    /**
     * @param array|\ArrayAccess|\Traversable $data
     * @return $this|Form
     */
    public function setData($data)
    {
        if (isset($data['blacklisted'])) {
            $data['blacklisted'] = (bool)$data['blacklisted'];
        }
        if (isset($data['value'])) {
            $data['value'] = FormHelper::clearValueField($data['value']);
        }
        parent::setData($data);
        $field = $this->get('type');
        if ($field) {
            if ($field->getValue() === Type::CPF) {
                $this->getInputFilter()->add(['name' => 'value', 'validators' => [['name' => Cpf::class]]]);
            } elseif ($field->getValue() === Type::CNPJ) {
                $this->getInputFilter()->add(['name' => 'value', 'validators' => [['name' => Cnpj::class]]]);
            }
        }
        return $this;
    }
}
