<?php
declare(strict_types=1);

namespace App\Infra\Form;

use Zend\InputFilter\InputFilter;

class LoadDocumentFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'id',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'string_expected'
                        ]
                    ]
                ]
            ]
        ]);
    }
}
