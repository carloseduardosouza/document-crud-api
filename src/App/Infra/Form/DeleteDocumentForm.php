<?php
declare(strict_types=1);

namespace App\Infra\Form;

use Zend\Form\Form;

class DeleteDocumentForm extends Form
{
    /**
     * DeleteDocumentForm constructor.
     */
    public function __construct()
    {
        parent::__construct('delete-document');
        $this->setInputFilter(new DeleteDocumentFilter());
        $this->add(['name' => 'id']);
    }
}
