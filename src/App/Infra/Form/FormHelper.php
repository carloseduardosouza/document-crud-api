<?php
declare(strict_types=1);

namespace App\Infra\Form;

class FormHelper
{
    /**
     * @param string $value
     * @return string
     */
    public static function clearValueField(string $value): string
    {
        return str_replace(['.', '-', '/', '\\', '|', ' '], '', $value);
    }
}
