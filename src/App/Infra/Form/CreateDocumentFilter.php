<?php
declare(strict_types=1);

namespace App\Infra\Form;

use App\Domain\Enum\Type;
use App\Infra\Validator\BooleanValidator;
use Zend\InputFilter\InputFilter;
use Zend\Validator\InArray;
use Zend\Validator\NotEmpty;

class CreateDocumentFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'type',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'string_expected'
                        ]
                    ]
                ],
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => Type::toArray(),
                        'strict' => InArray::COMPARE_STRICT,
                        'messages' => [
                            'notInArray' => 'invalid_document_type'
                        ]
                    ]
                ]
            ]
        ]);

        $this->add([
            'name' => 'value',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'string_expected'
                        ]
                    ]
                ]
            ]
        ]);

        $this->add([
            'name' => 'blacklisted',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'type' => NotEmpty::NULL,
                        'messages' => [
                            'isEmpty' => 'boolean_expected'
                        ]
                    ]
                ],
                [
                    'name' => BooleanValidator::class
                ]
            ]
        ]);
    }
}
