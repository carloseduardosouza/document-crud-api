<?php
declare(strict_types=1);

namespace App\Infra\Form;

use App\Domain\Enum\Type;
use App\Infra\Validator\BooleanValidator;
use App\Infra\Validator\Cnpj;
use App\Infra\Validator\Cpf;
use Zend\Form\Form;
use Zend\Validator\InArray;
use Zend\Validator\NotEmpty;

class ListDocumentForm extends Form
{
    /**
     * ListDocumentForm constructor.
     */
    public function __construct()
    {
        parent::__construct('list-document');
        $this->add(['name' => 'type']);
        $this->add(['name' => 'value']);
        $this->add(['name' => 'blacklisted']);
        $this->add(['name' => 'order']);
        $this->add(['name' => 'sense']);
    }

    /**
     * @param array|\ArrayAccess|\Traversable $data
     * @return $this|Form
     */
    public function setData($data)
    {
        if (isset($data['type'])) {
            $this->getInputFilter()->add([
                'name' => 'type',
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => Type::toArray(),
                            'strict' => InArray::COMPARE_STRICT,
                            'messages' => [
                                'notInArray' => 'invalid_document_type'
                            ]
                        ]
                    ]
                ]
            ]);
        }
        $hasValue = false;
        if (isset($data['value'])) {
            $data['value'] = FormHelper::clearValueField($data['value']);
            $this->getInputFilter()->add([
                'name' => 'type',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                'isEmpty' => 'string_expected'
                            ]
                        ]
                    ]
                ]
            ]);
            $hasValue = true;
        }
        if (isset($data['blacklisted'])) {
            $this->getInputFilter()->add([
                'name' => 'blacklisted',
                'validators' => [
                    [
                        'name' => 'NotEmpty',
                        'options' => [
                            'type' => NotEmpty::NULL,
                            'messages' => [
                                'isEmpty' => 'boolean_expected'
                            ]
                        ]
                    ],
                    [
                        'name' => BooleanValidator::class
                    ]
                ]
            ]);
            $data['blacklisted'] = (bool)$data['blacklisted'];
        }
        if (isset($data['order'])) {
            $this->getInputFilter()->add([
                'name' => 'order',
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => ['type', 'value', 'blacklisted', 'aud_dh_insert'],
                            'strict' => InArray::COMPARE_STRICT,
                            'messages' => [
                                'notInArray' => 'invalid_order_field'
                            ]
                        ]
                    ]
                ]
            ]);
        }
        if (isset($data['sense'])) {
            $this->getInputFilter()->add([
                'name' => 'sense',
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => ['asc', 'desc'],
                            'strict' => InArray::COMPARE_STRICT,
                            'messages' => [
                                'notInArray' => 'invalid_order_sense'
                            ]
                        ]
                    ]
                ]
            ]);
        }
        parent::setData($data);
        $field = $this->get('type');
        if ($field && $hasValue) {
            if ($field->getValue() === Type::CPF) {
                $this->getInputFilter()->add(['name' => 'value', 'validators' => [['name' => Cpf::class]]]);
            } elseif ($field->getValue() === Type::CNPJ) {
                $this->getInputFilter()->add(['name' => 'value', 'validators' => [['name' => Cnpj::class]]]);
            }
        }
        return $this;
    }
}
