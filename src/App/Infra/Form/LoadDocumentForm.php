<?php
declare(strict_types=1);

namespace App\Infra\Form;

use Zend\Form\Form;

class LoadDocumentForm extends Form
{
    /**
     * LoadDocumentForm constructor.
     */
    public function __construct()
    {
        parent::__construct('load-document');
        $this->setInputFilter(new DeleteDocumentFilter());
        $this->add(['name' => 'id']);
    }
}
