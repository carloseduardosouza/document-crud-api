<?php
declare(strict_types=1);

namespace App\Infra\Middleware;

use App\Domain\Exception\ApplicationException;
use ErrorException;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;
use Zend\Diactoros\Response\JsonResponse;

class ErrorMiddleware implements MiddlewareInterface
{
    /**
     * @var string format
     */
    const LOG_FORMAT = '%d [%s] %s: %s - File: %s - Line: %d';

    /**
     *
     * @var Logger
     */
    protected $logger;

    /**
     *
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        set_error_handler(function ($errno, $errstr, $errfile, $errline) {
            if (! (error_reporting() & $errno)) {
                // Error is not in mask
                return;
            }
            throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
        });

        try {
            $response = $handler->handle($request);
            return $response;
        } catch (Throwable $e) {
        }

        restore_error_handler();
        if ($e instanceof ApplicationException) {
            $data = $e->getData();
        } else {
            $data = ['message' => $e->getMessage()];
        }
        $response = new JsonResponse($data, $this->isValidErrorCode($e->getCode()) ? $e->getCode() : 500);
        $this->log($e, $request, $response);
        return $response;
    }

    /**
     * @param int $code
     * @return bool
     */
    private function isValidErrorCode(int $code): bool
    {
        return $code > 99 && $code < 600;
    }

    /**
     * @param Throwable $error
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     */
    private function log(\Throwable $error, ServerRequestInterface $request, ResponseInterface $response)
    {
        $this->logger->error(sprintf(
            self::LOG_FORMAT,
            $response->getStatusCode(),
            $request->getMethod(),
            (string) $request->getUri(),
            $error->getMessage(),
            $error->getFile(),
            $error->getLine()
        ));
    }
}
