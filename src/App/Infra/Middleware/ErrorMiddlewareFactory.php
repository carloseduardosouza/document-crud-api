<?php
declare(strict_types=1);

namespace App\Infra\Middleware;

use Monolog\Logger;
use Psr\Container\ContainerInterface;

class ErrorMiddlewareFactory
{
    /**
     * @param ContainerInterface $container
     * @return ErrorMiddleware
     */
    public function __invoke(ContainerInterface $container): ErrorMiddleware
    {
        return new ErrorMiddleware($container->get(Logger::class));
    }
}
