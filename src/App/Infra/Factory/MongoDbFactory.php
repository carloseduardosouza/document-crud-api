<?php
declare(strict_types=1);

namespace App\Infra\Factory;

use MongoDB\Client;
use MongoDB\Database;
use Psr\Container\ContainerInterface;

class MongoDbFactory
{
    /**
     * @var Database
     */
    private static $instance;

    /**
     * @param ContainerInterface $container
     * @return Database
     */
    public function __invoke(ContainerInterface $container): Database
    {
        if (is_null(self::$instance)) {
            $uri = getenv('MONGO_HOST');
            $uriOptions = [
                'username' => getenv('MONGO_USER'),
                'password' => getenv('MONGO_PWD')
            ];
            $driverOptions = [];
            $client = new Client($uri, $uriOptions, $driverOptions);
            self::$instance = $client->selectDatabase(getenv('MONGO_DBNAME'));
        }
        return self::$instance;
    }
}
