<?php
declare(strict_types=1);

namespace App\Infra\Factory;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;

class LoggerFactory
{
    /**
     * @param ContainerInterface $container
     * @return Logger
     * @throws \Exception
     */
    public function __invoke(ContainerInterface $container)
    {
        $log = new Logger('dibriway');
        $log->pushHandler(new StreamHandler(APP_PATH.'/logs/error.log', Logger::ERROR));
        return $log;
    }
}
