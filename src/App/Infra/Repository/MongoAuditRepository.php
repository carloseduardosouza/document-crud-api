<?php
declare(strict_types=1);

namespace App\Infra\Repository;

use App\Domain\Repository\AuditRepository;

class MongoAuditRepository extends AbstractMongoRepository implements AuditRepository
{

    /**
     * @return string
     */
    protected function getEntity(): string
    {
        return 'audit';
    }

    /**
     * @param \DateTime|null $limitDate
     * @return int
     */
    public function countInsert(?\DateTime $limitDate): int
    {
        $filter = [
            'type' => 'insert'
        ];
        if ($limitDate) {
            $filter['date'] = ['$gt' => $limitDate->getTimestamp()];
        }
        return $this->getCollection()->count($filter);
    }

    /**
     * @param \DateTime|null $limitDate
     * @return int
     */
    public function countUpdate(?\DateTime $limitDate): int
    {
        $filter = [
            'type' => 'update'
        ];
        if ($limitDate) {
            $filter['date'] = ['$gt' => $limitDate->getTimestamp()];
        }
        return $this->getCollection()->count($filter);
    }

    /**
     * @param \DateTime|null $limitDate
     * @return int
     */
    public function countDelete(?\DateTime $limitDate): int
    {
        $filter = [
            'type' => 'delete'
        ];
        if ($limitDate) {
            $filter['date'] = ['$gt' => $limitDate->getTimestamp()];
        }
        return $this->getCollection()->count($filter);
    }

    /**
     * @param \DateTime|null $limitDate
     * @return int
     */
    public function countSearch(?\DateTime $limitDate): int
    {
        $filter = [
            'type' => 'search'
        ];
        if ($limitDate) {
            $filter['date'] = ['$gt' => $limitDate->getTimestamp()];
        }
        return $this->getCollection()->count($filter);
    }
}
