<?php
declare(strict_types=1);

namespace App\Infra\Repository;

use MongoDB\Database;
use Psr\Container\ContainerInterface;

class MongoDocumentRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return MongoDocumentRepository
     */
    public function __invoke(ContainerInterface $container): MongoDocumentRepository
    {
        return new MongoDocumentRepository($container->get(Database::class));
    }
}
