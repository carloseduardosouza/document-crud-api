<?php
declare(strict_types=1);

namespace App\Infra\Repository;

use MongoDB\BSON\ObjectId;
use MongoDB\Collection;
use MongoDB\Database;
use MongoDB\Driver\Cursor;
use MongoDB\InsertOneResult;
use MongoDB\Model\BSONArray;
use MongoDB\Model\BSONDocument;
use MongoDB\UpdateResult;
use MongoDB\Driver\Exception\InvalidArgumentException;

abstract class AbstractMongoRepository
{
    /**
     * @var Database
     */
    protected $database;

    /**
     * @var Collection
     */
    private $collection;

    /**
     * AbstractMongoRepository constructor.
     * @param Database $database
     */
    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    /**
     * @return string
     */
    abstract protected function getEntity() :string;

    /**
     * @return Collection
     */
    protected function getCollection(): Collection
    {
        if (!$this->collection) {
            $this->collection = $this->database->selectCollection($this->getEntity());
        }
        return $this->collection;
    }

    /**
     * @param array $data
     * @return InsertOneResult
     * @throws \Exception
     */
    protected function insertOne(array $data): InsertOneResult
    {
        if (array_key_exists('id', $data)) {
            unset($data['id']);
        }
        $data['aud_dh_insert'] = (new \DateTime('now'))->getTimestamp();
        $result = $this->getCollection()->insertOne($data);
        $this->audit([
            'type' => 'insert',
            'entity' => $this->getEntity(),
            'entity_id' => (string)$result->getInsertedId(),
            'data' => $data,
            'date' => $data['aud_dh_insert']
        ]);
        return $result;
    }

    /**
     * @param string $id
     * @param array $data
     * @return UpdateResult
     * @throws \Exception
     */
    protected function updateOne(string $id, array $data): UpdateResult
    {
        if (array_key_exists('id', $data)) {
            unset($data['id']);
        }
        $data['aud_dh_update'] = (new \DateTime('now'))->getTimestamp();
        $result = $this->getCollection()->updateOne(['_id'=> new ObjectId($id),], ['$set' => $data]);
        $this->audit([
            'type' => 'update',
            'entity' => $this->getEntity(),
            'entity_id' => $id,
            'data' => $data,
            'date' => $data['aud_dh_update']
        ]);
        return $result;
    }

    /**
     * @param string $id
     * @throws \Exception
     */
    protected function deleteOne(string $id): void
    {
        $this->getCollection()->deleteOne(['_id' => new ObjectId($id)]);
        $this->audit([
            'type' => 'delete',
            'entity' => $this->getEntity(),
            'entity_id' => $id,
            'data' => [],
            'date' => (new \DateTime('now'))->getTimestamp()
        ]);
    }

    /**
     * @param array $filter
     * @param array $order
     * @return Cursor
     * @throws \Exception
     */
    protected function search(array $filter = [], $order = []): Cursor
    {
        $data = $this->getCollection()->find($filter, ['sort' => $order]);
        $this->audit([
            'type' => 'search',
            'entity' => $this->getEntity(),
            'filter' => $filter,
            'order' => $order,
            'date' => (new \DateTime('now'))->getTimestamp()
        ]);
        return $data;
    }

    /**
     * @param BSONDocument $document
     * @return array
     */
    protected function parse(BSONDocument $document): array
    {
        $data = [];
        foreach ($document->getArrayCopy() as $key => $value) {
            if ($value instanceof BSONDocument) {
                $data[$key] = $this->parse($value);
            } elseif ($value instanceof ObjectId) {
                $data['id'] = (string)((array)$value)['oid'];
            } elseif ($value instanceof BSONArray) {
                $data[$key] = $value->getArrayCopy();
            } else {
                if ($key !== 'id') {
                    $data[$key] = $value;
                }
            }
        }
        return $data;
    }

    /**
     * @param string $id
     * @return BSONDocument|null
     * @throws \Exception
     */
    protected function loadById(string $id): ?BSONDocument
    {
        try {
            $result = current($this->search(['_id' => new ObjectId($id)], [])->toArray());
            return $result === false ? null : $result;
        } catch (InvalidArgumentException $e) {
            return null;
        }
    }

    /**
     * @param array $data
     */
    private function audit(array $data): void
    {
        $this->database->selectCollection('audit')->insertOne($data);
    }
}
