<?php
declare(strict_types=1);

namespace App\Infra\Repository;

use MongoDB\Database;
use Psr\Container\ContainerInterface;

class MongoAuditRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return MongoAuditRepository
     */
    public function __invoke(ContainerInterface $container): MongoAuditRepository
    {
        return new MongoAuditRepository($container->get(Database::class));
    }
}
