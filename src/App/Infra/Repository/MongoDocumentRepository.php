<?php
declare(strict_types=1);

namespace App\Infra\Repository;

use App\Domain\Model\Document;
use App\Domain\Repository\DocumentRepository;
use MongoDB\Model\BSONDocument;

class MongoDocumentRepository extends AbstractMongoRepository implements DocumentRepository
{
    /**
     * @return string
     */
    protected function getEntity(): string
    {
        return 'document';
    }

    /**
     * @param Document $document
     * @return Document
     * @throws \Exception
     */
    public function store(Document $document): Document
    {
        $data = $document->toArray();
        $data['blacklisted'] = (int)$data['blacklisted'];
        $result = $this->insertOne($data);
        $document->setId((string)$result->getInsertedId());
        return $document;
    }

    /**
     * @param Document $document
     * @return Document
     * @throws \Exception
     */
    public function update(Document $document): Document
    {
        $data = $document->toArray();
        $data['blacklisted'] = (int)$data['blacklisted'];
        $this->updateOne($document->getId(), $data);
        return $document;
    }

    /**
     * @param Document $document
     * @throws \Exception
     */
    public function delete(Document $document): void
    {
        $this->deleteOne($document->getId());
    }

    /**
     * @param array $filter
     * @param array $order
     * @return array
     * @throws \Exception
     */
    public function list(array $filter = [], array $order = []): array
    {
        $data = [];
        if (isset($filter['blacklisted'])) {
            $filter['blacklisted'] = (int)$filter['blacklisted'];
        }
        foreach ($this->search($filter, $order) as $item) {
            $data[] = $this->parseDocument($item);
        }
        return $data;
    }

    /**
     * @param string $id
     * @return Document|null
     * @throws \Exception
     */
    public function load(string $id): ?Document
    {
        $data = $this->loadById($id);
        if ($data === null) {
            return null;
        }
        return $this->parseDocument($data);
    }

    /**
     * @param BSONDocument $document
     * @return Document
     */
    private function parseDocument(BSONDocument $document): Document
    {
        $params = $this->parse($document);
        $params['blacklisted'] = (bool)$params['blacklisted'];
        return new Document($params);
    }

    /**
     * @param Document $document
     * @return bool
     */
    public function exists(Document $document): bool
    {
        $result = $this->getCollection()
            ->findOne([
                'type' => $document->getType(),
                'value' => $document->getValue()
            ]);
        return $result !== null;
    }

    /**
     * @param Document $document
     * @return bool
     */
    public function isUniq(Document $document): bool
    {
        $result = $this->getCollection()
            ->findOne([
                'type' => $document->getType(),
                'value' => $document->getValue()
            ]);
        if ($result === null) {
            return true;
        }
        return ((array)$result['_id'])['oid'] === $document->getId();
    }
}
