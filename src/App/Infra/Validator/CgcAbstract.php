<?php
declare(strict_types=1);

namespace App\Infra\Validator;

use Zend\Validator\AbstractValidator;

abstract class CgcAbstract extends AbstractValidator
{
    /**
     * @var string
     */
    const NOT_VALID_DOCUMENT = 'isNotValidDocument';
    /**
     * @var int
     */
    protected $size = 0;

    /**
     * @var array
     */
    protected $modifiers = [];
    /**
     * @var string
     */
    protected $messageTemplates = [
        self::NOT_VALID_DOCUMENT => "not_valid_document"
    ];

    /**
     * @param string $value
     * @return bool
     */
    protected function check(string $value)
    {
        foreach ($this->modifiers as $modifier) {
            $result = 0;
            $size = count($modifier);
            for ($i = 0; $i < $size; $i++) {
                $result += $value[$i] * $modifier[$i];
            }
            $result = $result % 11;
            $digit = ($result < 2 ? 0 : 11 - $result);
            if ($value[$size] != $digit) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public function isValid($value)
    {
        $data = preg_replace('/[^0-9]/', '', $value);
        if (strlen($data) != $this->size) {
            $this->error(self::NOT_VALID_DOCUMENT);
            return false;
        }
        if (str_repeat($data[0], $this->size) == $data) {
            $this->error(self::NOT_VALID_DOCUMENT);
            return false;
        }
        if (!$this->check($data)) {
            $this->error(self::NOT_VALID_DOCUMENT);
            return false;
        }
        return true;
    }
}
