<?php
declare(strict_types=1);

namespace App\Infra\Validator;

class Cpf extends CgcAbstract
{
    /**
     * @var int
     */
    protected $size = 11;

    /**
     * @var array
     */
    protected $modifiers = [
        [10, 9, 8, 7, 6, 5, 4, 3, 2],
        [11, 10, 9, 8, 7, 6, 5, 4, 3, 2]
    ];

}

/*Exemplo de uso:
 * public function indexAction()
{
    $messagesCpf = [];
    $messagesCnpj = [];
    if ($this->getRequest()->isPost()) {
        $dataCpf = $this->params()->fromPost('cpf');
        $dataCnpj = $this->params()->fromPost('cnpj');

        $cpfValidator = new Cpf(['valid_if_empty' => true]);
        $cnpjValidator = new Cnpj(['valid_if_empty' => true]);

        $cpfValidator->isValid($dataCpf);
        $cnpjValidator->isValid($dataCnpj);

        $messagesCpf = $cpfValidator->getMessages();
        $messagesCnpj = $cnpjValidator->getMessages();
    }
    return new ViewModel([
        'messagesCpf' => $messagesCpf,
        'messagesCnpj' => $messagesCnpj
    ]);
}
 *
 *
 */
