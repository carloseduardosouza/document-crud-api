<?php
declare(strict_types=1);

namespace App\Infra\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class BooleanValidator extends AbstractValidator
{
    const NOT_BOOLEAN = 'NotBoolean';
    /**
     * @var array
     */
    protected $messageTemplates = [
        self::NOT_BOOLEAN => "boolean_expected"
    ];
    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        if (is_bool($value) === false) {
            $this->error(self::NOT_BOOLEAN);
            return false;
        }
        return true;
    }
}
