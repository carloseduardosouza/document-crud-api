<?php
declare(strict_types=1);

namespace App\Domain\Model;

use App\Domain\Enum\Type;
use App\Domain\Exception\DomainViolationException;
use App\Domain\Helper\Hydrator;

class Document
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $value;

    /**
     * @var bool
     */
    private $blacklisted;

    /**
     * Document constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        Hydrator::hydrate($data, $this);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Document
     */
    public function setId(string $id): Document
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Document
     * @throws DomainViolationException
     */
    public function setType(string $type): Document
    {
        if (!in_array($type, Type::toArray(), true)) {
            throw new DomainViolationException('invalid_document_type');
        }
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return Document
     */
    public function setValue(string $value): Document
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBlacklisted(): bool
    {
        return $this->blacklisted;
    }

    /**
     * @param bool $blacklisted
     * @return Document
     */
    public function setBlacklisted(bool $blacklisted): Document
    {
        $this->blacklisted = $blacklisted;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'value' => $this->value,
            'blacklisted' => $this->blacklisted
        ];
    }
}
