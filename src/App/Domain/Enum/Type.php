<?php
declare(strict_types=1);

namespace App\Domain\Enum;

final class Type
{
    const CPF = 'cpf';
    const CNPJ = 'cnpj';

    /**
     * @return array
     */
    public static function toArray(): array
    {
        return [self::CPF, self::CNPJ];
    }
}
