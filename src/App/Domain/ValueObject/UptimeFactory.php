<?php
declare(strict_types=1);

namespace App\Domain\ValueObject;

class UptimeFactory
{
    /**
     * @return Uptime
     * @throws \Exception
     */
    public static function factory(): Uptime
    {
        return new Uptime((float)explode(' ', file_get_contents( '/proc/uptime' ))[0]);
    }
}
