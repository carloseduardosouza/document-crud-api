<?php
declare(strict_types=1);

namespace App\Domain\ValueObject;

use DateInterval;

class Uptime extends DateInterval
{
    /**
     * @var float
     */
    protected $seconds;
    /**
     * Uptime constructor.
     * @param float $time
     * @throws \Exception
     */
    public function __construct(float $time)
    {
        $this->seconds = $time;
        list($days, $time)    = [(int) ($time / 86400), $time % 86400];
        list($hours, $time)   = [(int) ($time / 3600), $time % 3600];
        list($minutes, $time) = [(int) ($time / 60), $time % 60];
        $seconds = $time;
        parent::__construct("P{$days}DT{$hours}H{$minutes}M{$seconds}S");
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->seconds;
    }

    /**
     * @return float
     * @throws \Exception
     */
    public function getTotalMinutes(): float
    {
        $now = new \DateTime('now');
        $diff = clone $now;
        $diff->sub(clone $this);
        return abs($now->getTimestamp() - $diff->getTimestamp()) / 60;
    }
}
