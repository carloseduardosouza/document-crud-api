<?php
declare(strict_types=1);

namespace App\Domain\UseCase\LoadDocument;

use App\Domain\Repository\DocumentRepository;
use Psr\Container\ContainerInterface;

class LoadDocumentFactory
{
    /**
     * @param ContainerInterface $container
     * @return LoadDocument
     */
    public function __invoke(ContainerInterface $container): LoadDocument
    {
        return new LoadDocument($container->get(DocumentRepository::class));
    }
}
