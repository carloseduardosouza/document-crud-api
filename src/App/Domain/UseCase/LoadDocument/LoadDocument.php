<?php
declare(strict_types=1);

namespace App\Domain\UseCase\LoadDocument;

use App\Domain\Exception\DomainViolationException;
use App\Domain\Repository\DocumentRepository;

class LoadDocument
{
    /**
     * @var DocumentRepository
     */
    private $repository;

    /**
     * LoadDocument constructor.
     * @param DocumentRepository $repository
     */
    public function __construct(DocumentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param LoadDocumentRequest $request
     * @return LoadDocumentResponse
     * @throws DomainViolationException
     */
    public function handle(LoadDocumentRequest $request): LoadDocumentResponse
    {
        $document = $this->repository->load($request->getId());
        if ($document === null) {
            throw new DomainViolationException('document_not_found');
        }
        return new LoadDocumentResponse($document);
    }
}
