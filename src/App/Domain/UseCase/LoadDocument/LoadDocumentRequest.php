<?php
declare(strict_types=1);

namespace App\Domain\UseCase\LoadDocument;

use App\Domain\UseCase\UseCaseRequest;

class LoadDocumentRequest implements UseCaseRequest
{
    /**
     * @var string
     */
    private $id;

    /**
     * LoadDocumentRequest constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [];
    }
}
