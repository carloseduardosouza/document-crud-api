<?php
declare(strict_types=1);

namespace App\Domain\UseCase;

interface UseCaseResponse
{
    /**
     * @return string
     */
    public function getMessage(): string;

    /**
     * @return array
     */
    public function getData(): array;
}
