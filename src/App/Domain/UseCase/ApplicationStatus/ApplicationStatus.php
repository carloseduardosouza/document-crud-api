<?php
declare(strict_types=1);

namespace App\Domain\UseCase\ApplicationStatus;

use App\Domain\Repository\AuditRepository;
use App\Domain\ValueObject\Uptime;

class ApplicationStatus
{
    /**
     * @var AuditRepository
     */
    private $repository;

    /**
     * @var Uptime
     */
    private $uptime;

    /**
     * ApplicationStatus constructor.
     * @param AuditRepository $repository
     * @param Uptime $uptime
     */
    public function __construct(AuditRepository $repository, Uptime $uptime)
    {
        $this->repository = $repository;
        $this->uptime = $uptime;
    }


    /**
     * @param ApplicationStatusRequest $request
     * @return ApplicationsStatusResponse
     * @throws \Exception
     */
    public function handle(ApplicationStatusRequest $request): ApplicationsStatusResponse
    {
        $date = new \DateTime('now');
        $date->sub($this->uptime);
        return new ApplicationsStatusResponse(
            $this->uptime,
            $this->repository->countInsert($date),
            $this->repository->countUpdate($date),
            $this->repository->countDelete($date),
            $this->repository->countSearch($date)
        );
    }
}
