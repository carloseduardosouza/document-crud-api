<?php
declare(strict_types=1);

namespace App\Domain\UseCase\ApplicationStatus;

use App\Domain\Repository\AuditRepository;
use App\Domain\ValueObject\UptimeFactory;
use Psr\Container\ContainerInterface;

class ApplicationStatusFactory
{
    /**
     * @param ContainerInterface $container
     * @return ApplicationStatus
     * @throws \Exception
     */
    public function __invoke(ContainerInterface $container): ApplicationStatus
    {
        return new ApplicationStatus($container->get(AuditRepository::class), UptimeFactory::factory());
    }
}
