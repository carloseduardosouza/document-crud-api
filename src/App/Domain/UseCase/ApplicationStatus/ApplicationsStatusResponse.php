<?php
declare(strict_types=1);

namespace App\Domain\UseCase\ApplicationStatus;

use App\Domain\UseCase\UseCaseResponse;
use App\Domain\ValueObject\Uptime;

class ApplicationsStatusResponse implements UseCaseResponse
{
    /**
     * @var Uptime
     */
    private $uptime;

    /**
     * @var int
     */
    private $insert;

    /**
     * @var int
     */
    private $update;

    /**
     * @var int
     */
    private $delete;

    /**
     * @var int
     */
    private $search;

    /**
     * ApplicationsStatusResponse constructor.
     * @param Uptime $uptime
     * @param int $insert
     * @param int $update
     * @param int $delete
     * @param int $search
     */
    public function __construct(Uptime $uptime, int $insert, int $update, int $delete, int $search)
    {
        $this->uptime = $uptime;
        $this->insert = $insert;
        $this->update = $update;
        $this->delete = $delete;
        $this->search = $search;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'success';
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getData(): array
    {
        return [
            'uptime' => (int)$this->uptime->getTotalMinutes(),
            'insert' => $this->insert,
            'update' => $this->update,
            'delete' => $this->delete,
            'search' => $this->search
        ];
    }
}
