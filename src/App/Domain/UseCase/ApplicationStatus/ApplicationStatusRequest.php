<?php
declare(strict_types=1);

namespace App\Domain\UseCase\ApplicationStatus;

use App\Domain\UseCase\UseCaseRequest;

class ApplicationStatusRequest implements UseCaseRequest
{
    /**
     * @return array
     */
    public function toArray(): array
    {
        return [];
    }
}
