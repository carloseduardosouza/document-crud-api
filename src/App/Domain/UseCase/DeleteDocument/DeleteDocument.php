<?php
declare(strict_types=1);

namespace App\Domain\UseCase\DeleteDocument;

use App\Domain\Exception\DomainViolationException;
use App\Domain\Repository\DocumentRepository;

class DeleteDocument
{
    /**
     * @var DocumentRepository
     */
    private $repository;

    /**
     * DeleteDocument constructor.
     * @param DocumentRepository $repository
     */
    public function __construct(DocumentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param DeleteDocumentRequest $request
     * @return DeleteDocumentResponse
     * @throws DomainViolationException
     */
    public function handle(DeleteDocumentRequest $request): DeleteDocumentResponse
    {
        $document = $this->repository->load($request->getId());
        if ($document === null) {
            throw new DomainViolationException('document_not_found');
        }
        $this->repository->delete($document);
        return new DeleteDocumentResponse($request->getId());
    }
}
