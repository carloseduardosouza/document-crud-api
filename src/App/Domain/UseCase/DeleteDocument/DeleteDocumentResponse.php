<?php
declare(strict_types=1);

namespace App\Domain\UseCase\DeleteDocument;

use App\Domain\UseCase\UseCaseResponse;

class DeleteDocumentResponse implements UseCaseResponse
{
    /**
     * @var string
     */
    private $id;

    /**
     * DeleteDocumentResponse constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'document_deleted';
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return ['id' => $this->id];
    }
}
