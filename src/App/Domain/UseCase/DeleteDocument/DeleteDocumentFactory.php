<?php
declare(strict_types=1);

namespace App\Domain\UseCase\DeleteDocument;

use App\Domain\Repository\DocumentRepository;
use Psr\Container\ContainerInterface;

class DeleteDocumentFactory
{
    /**
     * @param ContainerInterface $container
     * @return DeleteDocument
     */
    public function __invoke(ContainerInterface $container): DeleteDocument
    {
        return new DeleteDocument($container->get(DocumentRepository::class));
    }
}
