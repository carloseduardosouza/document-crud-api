<?php
declare(strict_types=1);

namespace App\Domain\UseCase;

interface UseCaseRequest
{
    /**
     * @return array
     */
    public function toArray(): array;
}
