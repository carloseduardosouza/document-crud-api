<?php
declare(strict_types=1);

namespace App\Domain\UseCase\UpdateDocument;

use App\Domain\UseCase\UseCaseRequest;

class UpdateDocumentRequest implements UseCaseRequest
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $value;

    /**
     * @var bool
     */
    private $blacklisted;

    /**
     * UpdateDocumentRequest constructor.
     * @param string $id
     * @param string $type
     * @param string $value
     * @param bool $blacklisted
     */
    public function __construct(string $id, string $type, string $value, bool $blacklisted)
    {
        $this->id = $id;
        $this->type = $type;
        $this->value = $value;
        $this->blacklisted = $blacklisted;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'type' => $this->type,
            'value' => $this->value,
            'blacklisted' => $this->blacklisted
        ];
    }
}
