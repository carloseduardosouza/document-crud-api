<?php
declare(strict_types=1);

namespace App\Domain\UseCase\UpdateDocument;

use App\Domain\Model\Document;
use App\Domain\UseCase\UseCaseResponse;

class UpdateDocumentResponse implements UseCaseResponse
{
    /**
     * @var Document
     */
    private $document;

    /**
     * UpdateDocumentResponse constructor.
     * @param Document $document
     */
    public function __construct(Document $document)
    {
        $this->document = $document;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'document_updated';
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->document->toArray();
    }
}
