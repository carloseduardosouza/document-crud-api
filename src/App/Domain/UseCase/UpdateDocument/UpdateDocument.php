<?php
declare(strict_types=1);

namespace App\Domain\UseCase\UpdateDocument;

use App\Domain\Exception\DomainViolationException;
use App\Domain\Helper\Hydrator;
use App\Domain\Repository\DocumentRepository;

class UpdateDocument
{
    /**
     * @var DocumentRepository
     */
    private $repository;

    /**
     * UpdateDocument constructor.
     * @param DocumentRepository $repository
     */
    public function __construct(DocumentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param UpdateDocumentRequest $request
     * @return UpdateDocumentResponse
     * @throws DomainViolationException
     */
    public function handle(UpdateDocumentRequest $request): UpdateDocumentResponse
    {
        $document = $this->repository->load($request->getId());
        if ($document === null) {
            throw new DomainViolationException('document_not_found');
        }
        Hydrator::hydrate($request->toArray(), $document);
        if (!$this->repository->isUniq($document)) {
            throw new DomainViolationException('document_is_not_uniq');
        }
        $this->repository->update($document);
        return new UpdateDocumentResponse($document);
    }
}
