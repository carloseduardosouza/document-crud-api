<?php
declare(strict_types=1);

namespace App\Domain\UseCase\UpdateDocument;

use App\Domain\Repository\DocumentRepository;
use Psr\Container\ContainerInterface;

class UpdateDocumentFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdateDocument
     */
    public function __invoke(ContainerInterface $container): UpdateDocument
    {
        return new UpdateDocument($container->get(DocumentRepository::class));
    }
}
