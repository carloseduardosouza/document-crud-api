<?php
declare(strict_types=1);

namespace App\Domain\UseCase\CreateDocument;

use App\Domain\Exception\DomainViolationException;
use App\Domain\Model\Document;
use App\Domain\Repository\DocumentRepository;

class CreateDocument
{
    /**
     * @var DocumentRepository
     */
    private $repository;

    /**
     * CreateDocument constructor.
     * @param DocumentRepository $repository
     */
    public function __construct(DocumentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CreateDocumentRequest $request
     * @return CreateDocumentResponse
     * @throws DomainViolationException
     */
    public function handle(CreateDocumentRequest $request): CreateDocumentResponse
    {
        $document = new Document($request->toArray());
        if ($this->repository->exists($document)) {
            throw new DomainViolationException('document_already_exists');
        }
        $this->repository->store($document);
        return new CreateDocumentResponse($document);
    }
}
