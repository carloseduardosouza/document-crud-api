<?php
declare(strict_types=1);

namespace App\Domain\UseCase\CreateDocument;

use App\Domain\Model\Document;
use App\Domain\UseCase\UseCaseResponse;

class CreateDocumentResponse implements UseCaseResponse
{
    /**
     * @var Document
     */
    private $document;

    /**
     * CreateDocumentResponse constructor.
     * @param Document $document
     */
    public function __construct(Document $document)
    {
        $this->document = $document;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'document_created';
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->document->toArray();
    }
}
