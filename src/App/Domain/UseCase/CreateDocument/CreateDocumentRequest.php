<?php
declare(strict_types=1);

namespace App\Domain\UseCase\CreateDocument;

use App\Domain\UseCase\UseCaseRequest;

class CreateDocumentRequest implements UseCaseRequest
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $value;

    /**
     * @var bool
     */
    private $blacklisted;

    /**
     * CreateDocumentRequest constructor.
     * @param string $type
     * @param string $value
     * @param bool $blacklisted
     */
    public function __construct(string $type, string $value, bool $blacklisted)
    {
        $this->type = $type;
        $this->value = $value;
        $this->blacklisted = $blacklisted;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'type' => $this->type,
            'value' => $this->value,
            'blacklisted' => $this->blacklisted
        ];
    }
}
