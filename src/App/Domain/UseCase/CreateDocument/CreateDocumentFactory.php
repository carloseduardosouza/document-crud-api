<?php
declare(strict_types=1);

namespace App\Domain\UseCase\CreateDocument;

use App\Domain\Repository\DocumentRepository;
use Psr\Container\ContainerInterface;

class CreateDocumentFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreateDocument
     */
    public function __invoke(ContainerInterface $container): CreateDocument
    {
        return new CreateDocument($container->get(DocumentRepository::class));
    }
}
