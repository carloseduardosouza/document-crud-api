<?php
declare(strict_types=1);

namespace App\Domain\UseCase\ListDocument;

use App\Domain\Repository\DocumentRepository;

class ListDocument
{
    /**
     * @var DocumentRepository
     */
    private $repository;

    /**
     * ListDocument constructor.
     * @param DocumentRepository $repository
     */
    public function __construct(DocumentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param ListDocumentRequest $request
     * @return ListDocumentResponse
     */
    public function handle(ListDocumentRequest $request): ListDocumentResponse
    {
        return new ListDocumentResponse($this->repository->list($request->getFilter(), $request->getOrder()));
    }
}
