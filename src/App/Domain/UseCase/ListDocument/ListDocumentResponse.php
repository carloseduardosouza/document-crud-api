<?php
declare(strict_types=1);

namespace App\Domain\UseCase\ListDocument;

use App\Domain\UseCase\UseCaseResponse;

class ListDocumentResponse implements UseCaseResponse
{
    /**
     * @var array
     */
    private $data;

    /**
     * ListDocumentResponse constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'documents_listed';
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $items = [];
        foreach ($this->data as $document) {
            $items[] = $document->toArray();
        }
        return $items;
    }
}
