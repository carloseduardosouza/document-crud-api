<?php
declare(strict_types=1);

namespace App\Domain\UseCase\ListDocument;

use App\Domain\Repository\DocumentRepository;
use Psr\Container\ContainerInterface;

class ListDocumentFactory
{
    /**
     * @param ContainerInterface $container
     * @return ListDocument
     */
    public function __invoke(ContainerInterface $container): ListDocument
    {
        return new ListDocument($container->get(DocumentRepository::class));
    }
}
