<?php
declare(strict_types=1);

namespace App\Domain\UseCase\ListDocument;

use App\Domain\UseCase\UseCaseRequest;

class ListDocumentRequest implements UseCaseRequest
{
    /**
     * @var string|null
     */
    private $type;

    /**
     * @var string|null
     */
    private $value;

    /**
     * @var bool|null
     */
    private $blacklisted;

    /**
     * @var string|null
     */
    private $order;

    /**
     * @var string|null
     */
    private $sense;

    /**
     * ListDocumentRequest constructor.
     * @param string|null $type
     * @param string|null $value
     * @param bool|null $blacklisted
     * @param string|null $order
     * @param string|null $sense
     */
    public function __construct(
        ?string $type = null,
        ?string $value = null,
        ?bool $blacklisted = null,
        ?string $order = null,
        ?string $sense = null
    ) {
        $this->type = $type;
        $this->value = $value;
        $this->blacklisted = $blacklisted;
        $this->order = $order;
        $this->sense = $sense;
    }

    /**
     * @return array
     */
    public function getFilter(): array
    {
        $data = [];
        if ($this->type !== null) {
            $data['type'] = $this->type;
        }
        if ($this->value !== null) {
            $data['value'] = $this->value;
        }
        if ($this->blacklisted !== null) {
            $data['blacklisted'] = $this->blacklisted;
        }
        return $data;
    }

    /**
     * @return array
     */
    public function getOrder(): array
    {
        $data = [];
        if ($this->order !== null) {
            $data[$this->order] = $this->sense === 'desc' ? -1 : 1;
        }
        return $data;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [];
    }
}
