<?php
declare(strict_types=1);

namespace App\Domain\Exception;

abstract class ApplicationException extends \Exception
{
    /**
     * ApplicationException constructor.
     * @param string $message
     * @param int $code
     * @param \Throwable|null $throwable
     */
    public function __construct(string $message = '', int $code = 500, \Throwable $throwable = null)
    {
        parent::__construct($message, $code, $throwable);
    }

    /**
     * @return array
     */
    public abstract function getData(): array;
}
