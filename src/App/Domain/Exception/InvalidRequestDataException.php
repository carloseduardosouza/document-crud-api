<?php
declare(strict_types=1);

namespace App\Domain\Exception;

class InvalidRequestDataException extends ApplicationException
{
    const MESSAGE = 'validation_error';

    /**
     * @var array
     */
    private $errors;

    /**
     * InvalidRequestDataException constructor.
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        $this->errors = $errors;
        parent::__construct(self::MESSAGE);
    }

    /**
     * @return array
     */
    public function getValidationErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return [
            'message' => $this->getMessage(),
            'errors' => $this->getValidationErrors()
        ];
    }
}
