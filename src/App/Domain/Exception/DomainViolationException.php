<?php
declare(strict_types=1);

namespace App\Domain\Exception;

class DomainViolationException extends ApplicationException
{
    const MESSAGE = 'domain_violation_error';

    /**
     * DomainViolationException constructor.
     * @param string|null $message
     */
    public function __construct(string $message = null)
    {
        parent::__construct(($message === null) ? self::MESSAGE : $message);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return [
            'message' => $this->getMessage()
        ];
    }
}
