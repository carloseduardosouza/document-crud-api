<?php
declare(strict_types=1);

namespace App\Domain\Repository;

interface AuditRepository
{
    /**
     * @param \DateTime|null $limitDate
     * @return int
     */
    public function countInsert(?\DateTime $limitDate): int;

    /**
     * @param \DateTime|null $limitDate
     * @return int
     */
    public function countUpdate(?\DateTime $limitDate): int;

    /**
     * @param \DateTime|null $limitDate
     * @return int
     */
    public function countDelete(?\DateTime $limitDate): int;

    /**
     * @param \DateTime|null $limitDate
     * @return int
     */
    public function countSearch(?\DateTime $limitDate): int;
}
