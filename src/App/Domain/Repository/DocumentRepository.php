<?php
declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Model\Document;

interface DocumentRepository
{
    /**
     * @param Document $document
     * @return Document
     */
    public function store(Document $document): Document;

    /**
     * @param Document $document
     * @return Document
     */
    public function update(Document $document): Document;

    /**
     * @param Document $document
     */
    public function delete(Document $document): void;

    /**
     * @param array $filter
     * @param array $order
     * @return array
     */
    public function list(array $filter = [], array $order = []): array;

    /**
     * @param string $id
     * @return Document|null
     */
    public function load(string $id): ?Document;

    /**
     * @param Document $document
     * @return bool
     */
    public function exists(Document $document): bool;

    /**
     * @param Document $document
     * @return bool
     */
    public function isUniq(Document $document): bool;
}
