<?php
declare(strict_types=1);

namespace App\Domain\Helper;


final class Hydrator
{
    /**
     * @param array $data
     * @param object $object
     * @return object
     */
    public static function hydrate(array $data, object $object): object
    {
        $class = get_class($object);
        $methods = get_class_methods($class);
        foreach ($methods as $method) {
            preg_match(' /^(set)(.*?)$/i', $method, $results);
            $pre = $results[1]  ?? '';
            $k = $results[2]  ?? '';
            $k = strtolower(substr($k, 0, 1)) . substr($k, 1);
            if ($pre != 'set') {
                continue;
            }
            if (!isset($data[$k])) {
                continue;
            }
            $object->$method($data[$k]);
        }
        return $object;
    }
}
