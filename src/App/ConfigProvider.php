<?php

declare(strict_types=1);

namespace App;

use App\Domain\Repository\AuditRepository;
use App\Domain\Repository\DocumentRepository;
use App\Domain\UseCase\ApplicationStatus\ApplicationStatus;
use App\Domain\UseCase\ApplicationStatus\ApplicationStatusFactory;
use App\Domain\UseCase\CreateDocument\CreateDocument;
use App\Domain\UseCase\CreateDocument\CreateDocumentFactory;
use App\Domain\UseCase\DeleteDocument\DeleteDocument;
use App\Domain\UseCase\DeleteDocument\DeleteDocumentFactory;
use App\Domain\UseCase\ListDocument\ListDocument;
use App\Domain\UseCase\ListDocument\ListDocumentFactory;
use App\Domain\UseCase\LoadDocument\LoadDocument;
use App\Domain\UseCase\LoadDocument\LoadDocumentFactory;
use App\Domain\UseCase\UpdateDocument\UpdateDocument;
use App\Domain\UseCase\UpdateDocument\UpdateDocumentFactory;
use App\Handler\ApplicationStatusHandler;
use App\Handler\ApplicationStatusHandlerFactory;
use App\Handler\CreateDocumentHandler;
use App\Handler\CreateDocumentHandlerFactory;
use App\Handler\DeleteDocumentHandler;
use App\Handler\DeleteDocumentHandlerFactory;
use App\Handler\ListDocumentHandler;
use App\Handler\ListDocumentHandlerFactory;
use App\Handler\LoadDocumentHandler;
use App\Handler\LoadDocumentHandlerFactory;
use App\Handler\UpdateDocumentHandler;
use App\Handler\UpdateDocumentHandlerFactory;
use App\Infra\Factory\LoggerFactory;
use App\Infra\Factory\MongoDbFactory;
use App\Infra\Middleware\ErrorMiddleware;
use App\Infra\Middleware\ErrorMiddlewareFactory;
use App\Infra\Repository\MongoAuditRepositoryFactory;
use App\Infra\Repository\MongoDocumentRepositoryFactory;
use MongoDB\Database;
use Monolog\Logger;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array
    {
        return [
            'invokables' => [],
            'factories'  => [
                Database::class => MongoDbFactory::class,
                DocumentRepository::class => MongoDocumentRepositoryFactory::class,
                CreateDocument::class => CreateDocumentFactory::class,
                CreateDocumentHandler::class => CreateDocumentHandlerFactory::class,
                UpdateDocument::class => UpdateDocumentFactory::class,
                UpdateDocumentHandler::class => UpdateDocumentHandlerFactory::class,
                DeleteDocument::class => DeleteDocumentFactory::class,
                DeleteDocumentHandler::class => DeleteDocumentHandlerFactory::class,
                ListDocument::class => ListDocumentFactory::class,
                ListDocumentHandler::class => ListDocumentHandlerFactory::class,
                AuditRepository::class => MongoAuditRepositoryFactory::class,
                ApplicationStatus::class => ApplicationStatusFactory::class,
                ApplicationStatusHandler::class => ApplicationStatusHandlerFactory::class,
                LoadDocument::class => LoadDocumentFactory::class,
                LoadDocumentHandler::class => LoadDocumentHandlerFactory::class,
                Logger::class => LoggerFactory::class,
                ErrorMiddleware::class => ErrorMiddlewareFactory::class
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates() : array
    {
        return [];
    }
}
