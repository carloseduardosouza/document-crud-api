<?php
declare(strict_types=1);

namespace App\Handler;

use App\Domain\Exception\InvalidRequestDataException;
use App\Domain\UseCase\DeleteDocument\DeleteDocument;
use App\Domain\UseCase\DeleteDocument\DeleteDocumentRequest;
use App\Infra\Form\DeleteDocumentForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;

class DeleteDocumentHandler implements RequestHandlerInterface
{
    /**
     * @var DeleteDocument
     */
    private $useCase;

    /**
     * @var DeleteDocumentForm
     */
    private $form;

    /**
     * DeleteDocumentHandler constructor.
     * @param DeleteDocument $useCase
     * @param DeleteDocumentForm $form
     */
    public function __construct(DeleteDocument $useCase, DeleteDocumentForm $form)
    {
        $this->useCase = $useCase;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws InvalidRequestDataException
     * @throws \App\Domain\Exception\DomainViolationException
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $this->form->setData($request->getAttributes());
        if ($this->form->isValid() === false) {
            throw new InvalidRequestDataException($this->form->getMessages());
        }
        $ucRequest = new DeleteDocumentRequest($this->form->get('id')->getValue());
        return new JsonResponse($this->useCase->handle($ucRequest)->getData());
    }
}
