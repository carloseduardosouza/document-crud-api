<?php
declare(strict_types=1);

namespace App\Handler;

use App\Domain\UseCase\ListDocument\ListDocument;
use App\Infra\Form\ListDocumentForm;
use Psr\Container\ContainerInterface;

class ListDocumentHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return ListDocumentHandler
     */
    public function __invoke(ContainerInterface $container): ListDocumentHandler
    {
        return new ListDocumentHandler($container->get(ListDocument::class), new ListDocumentForm());
    }
}
