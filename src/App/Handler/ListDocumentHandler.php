<?php
declare(strict_types=1);

namespace App\Handler;

use App\Domain\Exception\InvalidRequestDataException;
use App\Domain\UseCase\ListDocument\ListDocument;
use App\Domain\UseCase\ListDocument\ListDocumentRequest;
use App\Infra\Form\ListDocumentForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;

class ListDocumentHandler implements RequestHandlerInterface
{
    /**
     * @var ListDocument
     */
    private $useCase;

    /**
     * @var ListDocumentForm
     */
    private $form;

    /**
     * ListDocumentHandler constructor.
     * @param ListDocument $useCase
     * @param ListDocumentForm $form
     */
    public function __construct(ListDocument $useCase, ListDocumentForm $form)
    {
        $this->useCase = $useCase;
        $this->form = $form;
    }


    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws InvalidRequestDataException
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $this->form->setData($request->getQueryParams());
        if ($this->form->isValid() === false) {
            throw new InvalidRequestDataException($this->form->getMessages());
        }
        $ucRequest = new ListDocumentRequest(
            $this->form->get('type')->getValue(),
            $this->form->get('value')->getValue(),
            $this->form->get('blacklisted')->getValue(),
            $this->form->get('order')->getValue(),
            $this->form->get('sense')->getValue()
        );
        return new JsonResponse($this->useCase->handle($ucRequest)->getData());
    }
}
