<?php
declare(strict_types=1);

namespace App\Handler;

use App\Domain\Exception\InvalidRequestDataException;
use App\Domain\UseCase\LoadDocument\LoadDocument;
use App\Domain\UseCase\LoadDocument\LoadDocumentRequest;
use App\Infra\Form\LoadDocumentForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;

class LoadDocumentHandler implements RequestHandlerInterface
{
    /**
     * @var LoadDocument
     */
    private $useCase;

    /**
     * @var LoadDocumentForm
     */
    private $form;

    /**
     * LoadDocumentHandler constructor.
     * @param LoadDocument $useCase
     * @param LoadDocumentForm $form
     */
    public function __construct(LoadDocument $useCase, LoadDocumentForm $form)
    {
        $this->useCase = $useCase;
        $this->form = $form;
    }
    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws InvalidRequestDataException
     * @throws \App\Domain\Exception\DomainViolationException
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $this->form->setData($request->getAttributes());
        if ($this->form->isValid() === false) {
            throw new InvalidRequestDataException($this->form->getMessages());
        }
        $ucRequest = new LoadDocumentRequest($this->form->get('id')->getValue());
        return new JsonResponse($this->useCase->handle($ucRequest)->getData());
    }
}
