<?php

declare(strict_types=1);

namespace App\Handler;

use App\Domain\Exception\InvalidRequestDataException;
use App\Domain\UseCase\CreateDocument\CreateDocument;
use App\Domain\UseCase\CreateDocument\CreateDocumentRequest;
use App\Infra\Form\CreateDocumentForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;

class CreateDocumentHandler implements RequestHandlerInterface
{
    /**
     * @var CreateDocument
     */
    private $useCase;

    /**
     * @var CreateDocumentForm
     */
    private $form;

    /**
     * CreateDocumentHandler constructor.
     * @param CreateDocument $useCase
     * @param CreateDocumentForm $form
     */
    public function __construct(CreateDocument $useCase, CreateDocumentForm $form)
    {
        $this->useCase = $useCase;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws InvalidRequestDataException
     * @throws \App\Domain\Exception\DomainViolationException
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $this->form->setData($request->getParsedBody());
        if ($this->form->isValid() === false) {
            throw new InvalidRequestDataException($this->form->getMessages());
        }
        $ucRequest = new CreateDocumentRequest(
            $this->form->get('type')->getValue(),
            $this->form->get('value')->getValue(),
            $this->form->get('blacklisted')->getValue()
        );
        return new JsonResponse($this->useCase->handle($ucRequest)->getData());
    }
}
