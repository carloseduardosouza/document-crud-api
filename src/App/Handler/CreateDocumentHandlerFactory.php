<?php
declare(strict_types=1);

namespace App\Handler;

use App\Domain\UseCase\CreateDocument\CreateDocument;
use App\Infra\Form\CreateDocumentForm;
use Psr\Container\ContainerInterface;

class CreateDocumentHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreateDocumentHandler
     */
    public function __invoke(ContainerInterface $container): CreateDocumentHandler
    {
        return new CreateDocumentHandler($container->get(CreateDocument::class), new CreateDocumentForm());
    }
}
