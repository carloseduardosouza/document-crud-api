<?php
declare(strict_types=1);

namespace App\Handler;

use App\Domain\UseCase\UpdateDocument\UpdateDocument;
use App\Infra\Form\UpdateDocumentForm;
use Psr\Container\ContainerInterface;

class UpdateDocumentHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdateDocumentHandler
     */
    public function __invoke(ContainerInterface $container): UpdateDocumentHandler
    {
        return new UpdateDocumentHandler($container->get(UpdateDocument::class), new UpdateDocumentForm());
    }
}
