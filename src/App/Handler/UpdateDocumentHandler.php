<?php
declare(strict_types=1);

namespace App\Handler;

use App\Domain\Exception\InvalidRequestDataException;
use App\Domain\UseCase\UpdateDocument\UpdateDocument;
use App\Domain\UseCase\UpdateDocument\UpdateDocumentRequest;
use App\Infra\Form\UpdateDocumentForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;

class UpdateDocumentHandler implements RequestHandlerInterface
{
    /**
     * @var UpdateDocument
     */
    private $useCase;

    /**
     * @var UpdateDocumentForm
     */
    private $form;

    /**
     * UpdateDocumentHandler constructor.
     * @param UpdateDocument $useCase
     * @param UpdateDocumentForm $form
     */
    public function __construct(UpdateDocument $useCase, UpdateDocumentForm $form)
    {
        $this->useCase = $useCase;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws InvalidRequestDataException
     * @throws \App\Domain\Exception\DomainViolationException
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $this->form->setData($request->getParsedBody());
        if ($this->form->isValid() === false) {
            throw new InvalidRequestDataException($this->form->getMessages());
        }
        $ucRequest = new UpdateDocumentRequest(
            $this->form->get('id')->getValue(),
            $this->form->get('type')->getValue(),
            $this->form->get('value')->getValue(),
            $this->form->get('blacklisted')->getValue()
        );
        return new JsonResponse($this->useCase->handle($ucRequest)->getData());
    }
}
