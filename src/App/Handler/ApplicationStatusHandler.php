<?php
declare(strict_types=1);

namespace App\Handler;

use App\Domain\UseCase\ApplicationStatus\ApplicationStatus;
use App\Domain\UseCase\ApplicationStatus\ApplicationStatusRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;

class ApplicationStatusHandler implements RequestHandlerInterface
{
    /**
     * @var ApplicationStatus
     */
    private $useCase;

    /**
     * ApplicationStatusHandler constructor.
     * @param ApplicationStatus $useCase
     */
    public function __construct(ApplicationStatus $useCase)
    {
        $this->useCase = $useCase;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $ucRequest = new ApplicationStatusRequest();
        return new JsonResponse($this->useCase->handle($ucRequest)->getData());
    }
}
