<?php
declare(strict_types=1);

namespace App\Handler;

use App\Domain\UseCase\DeleteDocument\DeleteDocument;
use App\Infra\Form\DeleteDocumentForm;
use Psr\Container\ContainerInterface;

class DeleteDocumentHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return DeleteDocumentHandler
     */
    public function __invoke(ContainerInterface $container): DeleteDocumentHandler
    {
        return new DeleteDocumentHandler($container->get(DeleteDocument::class), new DeleteDocumentForm());
    }
}
