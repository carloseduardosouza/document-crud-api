<?php
declare(strict_types=1);

namespace App\Handler;

use App\Domain\UseCase\LoadDocument\LoadDocument;
use App\Infra\Form\LoadDocumentForm;
use Psr\Container\ContainerInterface;

class LoadDocumentHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return LoadDocumentHandler
     */
    public function __invoke(ContainerInterface $container): LoadDocumentHandler
    {
        return new LoadDocumentHandler($container->get(LoadDocument::class), new LoadDocumentForm());
    }
}
