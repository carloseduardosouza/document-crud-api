<?php
declare(strict_types=1);

namespace App\Handler;

use App\Domain\UseCase\ApplicationStatus\ApplicationStatus;
use Psr\Container\ContainerInterface;

class ApplicationStatusHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return ApplicationStatusHandler
     */
    public function __invoke(ContainerInterface $container): ApplicationStatusHandler
    {
        return new ApplicationStatusHandler($container->get(ApplicationStatus::class));
    }
}
